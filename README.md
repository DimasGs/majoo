# Majoo Skill Test App

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## BUG
[BUG](https://drive.google.com/file/d/1UIekMO9K0juRHLwWAtb6o9Of_0e0r_aX/view?usp=sharing)
## Library Used
- [Cupertino Icons](https://pub.dev/packages/cupertino_icons)
- [Dio](https://pub.dev/packages/dio)
- [Sqflite](https://pub.dev/packages/sqflite)
- [Equatable](https://pub.dev/packages/equatable)
- [Flutter Screen Utils](https://pub.dev/packages/flutter_screenutil)
- [Flutter Bloc](https://pub.dev/packages/flutter_bloc)
- [Shared Preference](https://pub.dev/packages/shared_preferences)
- [Path Provider](https://pub.dev/packages/path_provider)
- [Path](https://pub.dev/packages/path)
- [Path Provider](https://pub.dev/packages/path_provider)
- [Pretty Dio Logger](https://pub.dev/packages/pretty_dio_logger)
- [Carousel Slider](https://pub.dev/packages/carousel_slider)
- [intl](https://pub.dev/packages/intl)
- [Flutter Rating Bar](https://pub.dev/packages/flutter_rating_bar)