import 'package:dio/dio.dart';
import 'package:majootestcase/configs/configs.dart';
import 'package:majootestcase/models/movie_model.dart';

class TopRatedMovieRepository {
  Dio dio = Dio();

  Future<List<Results>> getTopRatedMovies() async {
    try {
      Response response = await dio.get(Config.topRatedUrl);
      return response.data['results']
          .map<Results>((json) => Results.fromJson(json))
          .toList();
    } catch (e) {
      throw e;
    }
  }
}
