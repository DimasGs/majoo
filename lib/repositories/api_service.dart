import 'package:dio/dio.dart';
import 'package:majootestcase/configs/configs.dart';
import 'package:majootestcase/models/movie_model.dart';

class ApiServices {
  Dio dio = Dio();
  Future<MovieModel?> getMovieList() async {
    try {
      Response response = await dio.get(Config.popularUrl,
          options: Options(
            headers: {"Authorization": "Bearer ${Config.bearerToken}"},
          ));
      return MovieModel.fromJson(response.data);
    } catch (e) {
      print("getMovieList ERROR : ${e.toString()}");
      return null;
    }
  }
}
