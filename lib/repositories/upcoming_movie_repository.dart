import 'package:dio/dio.dart';
import 'package:majootestcase/configs/configs.dart';
import 'package:majootestcase/models/movie_model.dart';

class UpcomingMovieRepository {
  Dio dio = Dio();

  Future<List<Results>> getUpcomingMovies() async {
    try {
      Response response = await dio.get(Config.upcomingMovieUrl);
      return response.data['results']
          .map<Results>((json) => Results.fromJson(json))
          .toList();
    } catch (e) {
      throw e;
    }
  }
}
