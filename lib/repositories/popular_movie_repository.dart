import 'package:dio/dio.dart';
import 'package:majootestcase/configs/configs.dart';
import 'package:majootestcase/models/movie_model.dart';

class PopularMovieRepository {
  Dio dio = Dio();

  Future<List<Results>> getPopularMovies() async {
    try {
      Response response = await dio.get(Config.popularUrl);
      return response.data['results']
          .map<Results>((json) => Results.fromJson(json))
          .toList();
    } catch (e) {
      throw e;
    }
  }
}
