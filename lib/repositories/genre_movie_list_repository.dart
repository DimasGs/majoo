import 'package:dio/dio.dart';
import 'package:majootestcase/configs/configs.dart';
import 'package:majootestcase/models/movie_model.dart';

class GenreMovieListRepository {
  Dio dio = Dio();

  Future<List<Results>> getGenreMovieList(int genreId) async {
    try {
      Response response =
          await dio.get("${Config.genreMovieListUrl}&with_genres=$genreId");
      return response.data['results']
          .map<Results>((json) => Results.fromJson(json))
          .toList();
    } catch (e) {
      throw e;
    }
  }
}
