import 'package:majootestcase/bloc/genremovielist/genre_movie_list_cubit.dart';
import 'package:majootestcase/bloc/moviedetail/movie_detail_cubit.dart';
import 'package:majootestcase/bloc/popularmovie/popular_movie_cubit.dart';
import 'package:majootestcase/bloc/register/register_bloc_cubit.dart';
import 'package:majootestcase/bloc/topratedmovie/top_rated_movie_cubit.dart';
import 'package:majootestcase/bloc/upcomingmovie/upcoming_movie_cubit.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(
            lazy: false,
            create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
          ),
          BlocProvider<RegisterBlocCubit>(
            lazy: false,
            create: (context) => RegisterBlocCubit(),
          ),
          BlocProvider<MovieDetailCubit>(
            lazy: false,
            create: (context) => MovieDetailCubit(),
          ),
          BlocProvider<PopularMovieCubit>(
            create: (context) => PopularMovieCubit()..getPopularMovies(),
          ),
          BlocProvider<GenreMovieListCubit>(
            create: (context) => GenreMovieListCubit()..getGenreMovieList(),
          ),
          BlocProvider<TopRatedMovieCubit>(
            create: (context) => TopRatedMovieCubit()..getTopRatedMovie(),
          ),
          BlocProvider<UpcomingMovieCubit>(
            create: (context) => UpcomingMovieCubit()..getUpcomingMovies(),
          ),
        ],
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
      if (state is AuthBlocLoginState) {
        return LoginPage();
      } else if (state is AuthBlocLoggedInState) {
        return BlocProvider(
          create: (context) => HomeBlocCubit()..fetchingData(),
          child: HomeBlocScreen(),
        );
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}
