import 'package:bloc/bloc.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/repositories/upcoming_movie_repository.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'upcoming_movie_state.dart';

class UpcomingMovieCubit extends Cubit<UpcomingMovieState> {
  UpcomingMovieCubit() : super(UpcomingMovieInitial());

  UpcomingMovieRepository repository = UpcomingMovieRepository();

  Future<void> getUpcomingMovies() async {
    try {
      emit(UpcomingMovieLoadInProgress());
      final upcomingMovies = await repository.getUpcomingMovies();
      emit(UpcomingMovieLoadSuccess(upcomingMovies));
    } catch (e) {
      emit(UpcomingMovieLoadFailure());
    }
  }
}
