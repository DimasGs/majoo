import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/database/database_helper.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterState> {
  final dbHelper = DatabaseHelper.instance;

  RegisterBlocCubit() : super(RegisterInitial());

  Future<int?> insertUser({
    required String email,
    required String password,
  }) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnEmail: email,
      DatabaseHelper.columnPassWord: password,
    };
    final id = await dbHelper.insert(row);
    return id;
  }
}
