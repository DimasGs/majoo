import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/repositories/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());
  ApiServices apiServices = ApiServices();

  void fetchingData() async {
    emit(HomeBlocLoadingState());
    MovieModel? movieResponse = await apiServices.getMovieList();
    if (movieResponse == null) {
      emit(HomeBlocErrorState("Error Unknown"));
    } else {
      emit(HomeBlocLoadedState(movieResponse.results));
    }
  }
}
