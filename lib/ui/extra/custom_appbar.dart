import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final bool showSearchButton;

  CustomAppBar({this.showSearchButton = true});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text('Movies DB'),
      elevation: 0.0,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
