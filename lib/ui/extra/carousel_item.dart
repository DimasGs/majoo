import 'package:flutter/material.dart';

class CarouselItem extends StatelessWidget {
  final String avatar;
  final String title;
  final Function()? onTap;

  CarouselItem({
    required this.avatar,
    required this.title,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Image.network(
            'https://image.tmdb.org/t/p/w500/$avatar',
            fit: BoxFit.cover,
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                splashColor: Color.fromRGBO(
                  0,
                  0,
                  0,
                  0.3,
                ),
                highlightColor: Color.fromRGBO(
                  0,
                  0,
                  0,
                  0.1,
                ),
                onTap: onTap,
              ),
            ),
          ),
          Positioned(
            bottom: 30.0,
            left: 10.0,
            child: Text(
              title,
              style: TextStyle(
                fontSize: 18.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
