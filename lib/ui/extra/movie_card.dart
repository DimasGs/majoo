import 'package:flutter/material.dart';
import 'package:majootestcase/configs/configs.dart';

class MovieCard extends StatelessWidget {
  final String title;
  final String poster;
  final double? rating;
  final Function() onTap;
  final String? subtitle;

  MovieCard({
    required this.title,
    required this.poster,
    this.rating,
    this.subtitle,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.only(
          top: 8.0,
          right: 12.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(2.0),
              ),
              child: Stack(
                children: [
                  Image.network(
                    '${Config.baseImageUrl}$poster',
                    fit: BoxFit.cover,
                    width: 120.0,
                    height: 180.0,
                  ),
                  Positioned.fill(
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Color.fromRGBO(
                          0,
                          0,
                          0,
                          0.3,
                        ),
                        highlightColor: Color.fromRGBO(
                          0,
                          0,
                          0,
                          0.1,
                        ),
                        onTap: onTap,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 120,
              padding: EdgeInsets.only(
                top: 8.0,
              ),
              child: Text(
                title,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 13.0,
                  color: Colors.black,
                  // height: 1.3,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
