import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/genremovielist/genre_movie_list_cubit.dart';
import 'package:majootestcase/bloc/moviedetail/movie_detail_cubit.dart';
import 'package:majootestcase/bloc/popularmovie/popular_movie_cubit.dart';
import 'package:majootestcase/bloc/topratedmovie/top_rated_movie_cubit.dart';
import 'package:majootestcase/bloc/upcomingmovie/upcoming_movie_cubit.dart';
import 'package:majootestcase/models/genre.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie.dart';
import 'package:majootestcase/ui/extra/carousel_item.dart';
import 'package:majootestcase/ui/extra/custom_appbar.dart';
import 'package:majootestcase/ui/extra/dot_indicator.dart';
import 'package:majootestcase/ui/extra/movie_card.dart';
import 'package:majootestcase/ui/extra/section_header.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final List<Results>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<HomeBlocLoadedScreen>
    with SingleTickerProviderStateMixin {
  int _current = 0;
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(
      length: genres.length,
      vsync: this,
      initialIndex: 0,
    );
    _tabController.addListener(_handleTabIndex);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabIndex);
    _tabController.dispose();
    super.dispose();
  }

  void _handleTabIndex() {
    setState(() {});
  }

  void _getGenreListById(int id) {
    context
        .read<GenreMovieListCubit>()
        .getGenreMovieList(genreId: genres[id].id ?? 0);
  }

  void _onPressMovie(int movieId) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (context) => MovieDetailCubit(),
          child: DetailMoviePage(movieId: movieId),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BlocBuilder<PopularMovieCubit, PopularMovieState>(
              builder: (context, state) {
                if (state is PopularMovieLoadInProgress) {
                  return Container(
                    height: 220,
                    child: Center(
                      child: Transform.scale(
                        scale: 0.7,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.black,
                          ),
                        ),
                      ),
                    ),
                  );
                } else if (state is PopularMovieLoadSuccess) {
                  return Stack(
                    children: [
                      _buildCarouselSlider(state.popularMovies),
                      _buildCarouselIndicator(state.popularMovies),
                    ],
                  );
                } else {
                  return Text(
                    'Failed Get Data Banner',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  );
                }
              },
            ),
            _buildTabbar(),
            BlocBuilder<GenreMovieListCubit, GenreMovieListState>(
              builder: (context, state) {
                if (state is GenreMovieListLoadInProgress) {
                  return Container(
                    height: 250,
                    child: Center(
                      child: Transform.scale(
                        scale: 0.7,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.black,
                          ),
                        ),
                      ),
                    ),
                  );
                } else if (state is GenreMovieListLoadSuccess) {
                  return Container(
                    child: _buildTabBarView(state.genreMovieLists),
                  );
                } else {
                  return Text(
                    'Failed Get Data Tab',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  );
                }
              },
            ),
            BlocBuilder<TopRatedMovieCubit, TopRatedMovieState>(
              builder: (context, state) {
                if (state is TopRatedMovieLoadInProgress) {
                  return Container(
                    height: 250,
                    child: Center(
                      child: Transform.scale(
                        scale: 0.7,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.black,
                          ),
                        ),
                      ),
                    ),
                  );
                } else if (state is TopRatedMovieLoadSuccess) {
                  return _buildTopRatedMovie(state.topRatedMovies);
                } else {
                  return Text(
                    'Failed Get Data Top Rated Movies',
                    style: TextStyle(color: Colors.black),
                  );
                }
              },
            ),
            BlocBuilder<UpcomingMovieCubit, UpcomingMovieState>(
              builder: (context, state) {
                if (state is UpcomingMovieLoadInProgress) {
                  return Container(
                    height: 250,
                    child: Center(
                      child: Transform.scale(
                        scale: 0.7,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.black,
                          ),
                        ),
                      ),
                    ),
                  );
                } else if (state is UpcomingMovieLoadSuccess) {
                  return _buildUpcomingMovie(state.upcomingMovies);
                } else {
                  return Text(
                    'Failed Get Data Upcoming Movies',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  _buildCarouselSlider(List<Results> list) {
    return CarouselSlider(
      items: list.sublist(0, 5).map(
        (item) {
          return CarouselItem(
            avatar: item.backdropPath ?? "",
            title: "${item.title}",
            onTap: () => _onPressMovie(item.id ?? 0),
          );
        },
      ).toList(),
      options: CarouselOptions(
        autoPlay: true,
        viewportFraction: 1,
        enlargeCenterPage: false,
        onPageChanged: (index, reason) {
          setState(
            () {
              _current = index;
            },
          );
        },
      ),
    );
  }

  _buildCarouselIndicator(List<Results> list) {
    return DotIndicator(
      lists: list,
      currentIndex: _current,
    );
  }

  _buildTabbar() {
    return TabBar(
      controller: _tabController,
      onTap: (index) => _getGenreListById(index),
      labelColor: Colors.black,
      indicatorWeight: 3,
      indicatorColor: Colors.black,
      isScrollable: true,
      labelStyle: TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w600,
      ),
      tabs: genres.map(
        (item) {
          return Tab(
            text: item.name?.toUpperCase(),
          );
        },
      ).toList(),
    );
  }

  _buildTabBarView(List<Results> genreListMovies) {
    return Container(
      height: 250,
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: _tabController,
        children: genres.map((item) {
          return ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: genreListMovies.length,
            itemBuilder: (context, index) {
              Results data = genreListMovies[index];
              return MovieCard(
                title: data.title ?? "",
                poster: data.posterPath ?? "",
                onTap: () => _onPressMovie(data.id ?? 0),
              );
            },
            padding: EdgeInsets.symmetric(
              horizontal: 12.0,
            ),
          );
        }).toList(),
      ),
    );
  }

  _buildTopRatedMovie(List<Results> topRatedMovies) {
    return Column(
      children: [
        SectionHeader(
          title: 'Top Rated Movies',
        ),
        Container(
          height: 250,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: topRatedMovies.length,
            itemBuilder: (context, index) {
              Results data = topRatedMovies[index];
              return MovieCard(
                title: data.title ?? "",
                poster: data.posterPath ?? "",
                onTap: () => _onPressMovie(data.id ?? 0),
              );
            },
            padding: EdgeInsets.symmetric(
              horizontal: 12.0,
            ),
          ),
        ),
      ],
    );
  }

  _buildUpcomingMovie(List<Results> upcomingMovies) {
    return Column(
      children: [
        SectionHeader(
          title: 'Upcoming Movies',
        ),
        Container(
          height: 250,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: upcomingMovies.length,
            itemBuilder: (context, index) {
              Results data = upcomingMovies[index];
              return MovieCard(
                title: data.title ?? "",
                poster: data.posterPath ?? "",
                onTap: () => _onPressMovie(data.id ?? 0),
              );
            },
            padding: EdgeInsets.symmetric(
              horizontal: 12.0,
            ),
          ),
        ),
      ],
    );
  }
}
