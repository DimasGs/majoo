import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/register/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/already_have_account.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/ui/login/login_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RegisterPageState();
  }
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  final _formKey = GlobalKey<FormState>();
  bool _isObscurePassword = true;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: BlocListener<RegisterBlocCubit, RegisterState>(
      listener: (context, state) {
        if (state is RegisterUserExists) {
          SnackBar snackBar = const SnackBar(
            content: Text('User already exists'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }

        if (state is RegisterSuccess) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => LoginPage(),
            ),
          );
        }
      },
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  "SIGNUP",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: size.height * 0.03),
                CustomTextFormField(
                  context: context,
                  controller: _emailController,
                  hint: 'Example@123.com',
                  label: 'Email',
                  isEmail: true,
                  validator: (val) {
                    final pattern =
                        new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                    if (val != null)
                      return pattern.hasMatch(val) ? null : 'email is invalid';
                  },
                ),
                SizedBox(height: size.height * 0.03),
                CustomTextFormField(
                  context: context,
                  label: 'Password',
                  hint: 'password',
                  controller: _passwordController,
                  isObscureText: _isObscurePassword,
                  suffixIcon: IconButton(
                    icon: Icon(
                      _isObscurePassword
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined,
                    ),
                    onPressed: () {
                      setState(() {
                        _isObscurePassword = !_isObscurePassword;
                      });
                    },
                  ),
                ),
                SizedBox(height: size.height * 0.03),
                CustomButton(
                  text: 'Register',
                  onPressed: handleRegister,
                  height: 100,
                ),
                SizedBox(height: size.height * 0.03),
                AlreadyHaveAnAccountCheck(
                  login: false,
                  press: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => BlocProvider(
                          create: (context) => AuthBlocCubit(),
                          child: LoginPage(),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }

  void handleRegister() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    if (_formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      RegisterBlocCubit registerBloc = RegisterBlocCubit();
      registerBloc.insertUser(email: _email, password: _password);
    }
  }
}
