import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:majootestcase/bloc/moviedetail/movie_detail_cubit.dart';
import 'package:majootestcase/models/movie_detail.dart';
import 'package:majootestcase/ui/extra/carousel_item.dart';
import 'package:majootestcase/ui/extra/custom_appbar.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/utils/helpers.dart';

class DetailMoviePage extends StatefulWidget {
  final int movieId;
  DetailMoviePage({required this.movieId});
  @override
  _DetailMovieState createState() => _DetailMovieState();
}

class _DetailMovieState extends State<DetailMoviePage> {
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    context.read<MovieDetailCubit>().getMovieDetail(widget.movieId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: BlocBuilder<MovieDetailCubit, MovieDetailState>(
        builder: (context, state) {
          if (state is MovieDetailLoadInProgress) {
            return LoadingIndicator();
          } else if (state is MovieDetailLoadSuccess) {
            MovieDetail data = state.movieDetail;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _featuredImages(data.images?.backdrops),
                _movieTitleInfo(data),
                _divider(),
                _movieDescInfo(data),
                _divider(),
                _movieInfoStatus(data),
                _divider(),
              ],
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  Widget _featuredImages(List<Backdrop>? data) {
    return CarouselSlider(
      items: data?.map((item) {
        return CarouselItem(
          avatar: item.filePath ?? "",
          title: '',
          onTap: () {},
        );
      }).toList(),
      options: CarouselOptions(
        autoPlay: true,
        viewportFraction: 1,
        enlargeCenterPage: false,
      ),
    );
  }

  Widget _movieTitleInfo(MovieDetail data) {
    int year = DateTime.parse(data.releaseDate.toString()).year;
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 8,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  scrollDirection: Axis.horizontal,
                  child: Text(
                    "${data.title}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: 28.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ),
            ],
          ),
          RichText(
            text: TextSpan(
              text: '${year.toString()} ',
              style: TextStyle(fontSize: 12.0, color: Colors.black),
              children: <TextSpan>[
                TextSpan(text: '• '),
                TextSpan(
                  text: Helper.convertHoursMinutes(data.runtime ?? 0),
                  style: TextStyle(
                    fontSize: 12.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _movieDescInfo(MovieDetail data) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Image.network(
              "https://image.tmdb.org/t/p/w500/${data.posterPath}",
              height: 180.0,
              width: 120.0,
              fit: BoxFit.fill,
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Column(
                children: [
                  Container(
                    height: 30,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: data.genres?.length,
                      itemBuilder: (context, index) {
                        Genre genre = data.genres![index];
                        return Padding(
                          padding: EdgeInsets.only(right: 6.0),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black12,
                              ),
                              borderRadius: BorderRadius.circular(3.0),
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: 6.0,
                                horizontal: 12,
                              ),
                              child: Text(
                                "${genre.name}",
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      "${data.overview}",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 5,
                      style: TextStyle(
                        height: 1.4,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _movieInfoStatus(MovieDetail data) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Row(
        children: [
          Flexible(
            flex: 1,
            child: Center(
              child: Column(
                children: [
                  Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: RichText(
                      text: TextSpan(
                        text: '${data.voteAverage}',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: '/10',
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 4.0,
                    ),
                    child: Text(
                      'Rating',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 13.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Center(
              child: Column(
                children: [
                  Icon(
                    Icons.thumb_up_alt_outlined,
                    color: Colors.green,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      NumberFormat.compactCurrency(
                        locale: 'id',
                        decimalDigits: 0,
                        name: "IDR ",
                      ).format(data.popularity),
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 4.0,
                    ),
                    child: Text(
                      'Popularity',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 13.0,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Center(
              child: Column(
                children: [
                  Icon(
                    Icons.local_movies_outlined,
                    color: Colors.redAccent,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      "${data.status}",
                      style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 4.0,
                    ),
                    child: Text(
                      'Status',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 13.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _divider() {
    return Divider(
      color: Colors.black12,
      thickness: 1,
    );
  }
}
