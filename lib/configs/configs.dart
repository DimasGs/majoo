class Config {
  static String apiKey = "678815161909cb382c4aad58ebf7b085";
  static String baseUrl = "https://api.themoviedb.org/3";
  static String bearerToken =
      "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2Nzg4MTUxNjE5MDljYjM4MmM0YWFkNThlYmY3YjA4NSIsInN1YiI6IjYyNjAxNDI4ZGI5NTJkMTI2NmMzY2ZkNCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.FE1w76RaTUWEI2EU7nmPMkZdeFsJNA6gTTqHpBfjx_o";
  static String baseImageUrl = "https://image.tmdb.org/t/p/w500";

  static String popularUrl = '$baseUrl/movie/popular?api_key=$apiKey&page=1';
  static String genreMovieListUrl =
      '$baseUrl/discover/movie?api_key=$apiKey&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1';
  static String topRatedUrl =
      '$baseUrl/movie/top_rated?api_key=$apiKey&language=en-EN&page=1';
  static String upcomingMovieUrl =
      '$baseUrl/movie/upcoming?api_key=$apiKey&language=en-US&page=1';
  static movieDetailUrl(int movieId) =>
      '$baseUrl/movie/$movieId?api_key=$apiKey&append_to_response=images';
  static movieCreditlUrl(int movieId) =>
      '$baseUrl/movie/$movieId/credits?api_key=$apiKey';
  static similiarMovieUrl(int movieId) =>
      '$baseUrl/movie/$movieId/similar?api_key=$apiKey&page=1';
}
